/**
 * __main__:
 *
 * 是服务器启动的入口程序，其中包括了启动服务(run)，停止服务(stop)，重启服务(restart)
 *
 * User: ZhangXiao
 * Date: 14-4-23
 * Time: 下午1:20
 * File: __main__.js
 */


var util = require("util");
var fs = require("fs");
var path = require("path");

//全局配置，且只能在__main__中重新设置
var __config__ = require("./__config__").config;

/**
 *启动服务入口
 */
if (fs.existsSync(__config__.main)) {

    //加载服务模块
    var main = require(__config__.main).server;

    //启动服务器
    main.run(__config__);
} else {
    util.log(__config__.main + " Not Found .");
}




