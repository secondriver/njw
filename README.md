### NJW: *node java Web*

`Node.js`初相识，边学，边写，用`java`的思维写`node.js web`应用。
****

#### 1. Node.js是什么？

Node.js是一种让JavaScript运行在服务器端的开发平台。具体详见官方网站：[http://www.nodejs.org](http://www.nodejs.org "Node.js")

### 2. Node.js能做什么？
 
如果如1所述，可以看看官网对`Node.js`的介绍，加上对`JavaScript`的了解，大概应该知道`Node.js`能做些什么了。

我大概理解的是:*`JavaScript`语言+浏览器客户端以及浏览器客户端提供了客户端`JavaScript`*改善了和提升了前端交互和用户体验，*`node.js`是`javascript`+`node.js`平台为网络开发而生*。

这里应用官网的说法，`node.js`能够做什么？

+ 开发复杂逻辑的网站
+ WebSocket服务器
+ 命令行工具
+ 单元测试工具
+ TCP/UDP套接字应用
+ 等等

### 3. Node.js怎么学？

我本人不是前端开发者，也只是在工作中接触到和涉猎一些。网上有爆料说：“`node.js`的诞生直接是前端工程师直逼后端开发，后端开发者亚历山大喽。”

不过可以分享一下，一个不懂前端的Java开发者如何上手'Node.js'的经历。

+ 我大致浏览了一下《Nodejs开发指南》这本书，熟悉了一下`node.js`常用的几个API和一些简单的编程。
+ 百度，谷歌反复搜索`Node.js`,阅读了一些有关文档已经`node.js`的核心思想
+ 官方网站的[API](http://nodejs.org/api/ "node.js API")挑了一些做实验,比如：`OS`, `Path`, `Utilities`, `File System`, `Console`, `HTTP`等几个模块。
+ 接下来就边学编写`NJW`。


### 4. 为什么写NJW？


`NJW`不是`Node.js`应用，也不是`Node.js`开发的Web框架，当然更不是JavaWeb应用，而是Java开发者接触`Node.js`从0到0.1的一个过程。


起初看《Node.js开发指南》一书的时候，书中大篇幅介绍了[express](http://expressjs.com/ "node.js web 应用框架"),影响到我对`Node.js`的理解，`java`开发中的各种框架鳞次栉比，所以对框架这东西既爱又恨，爱之随手即来，恨之云里雾里。

于是就从官网的这个快速启动一个Web服务的例子开始了`node.js`web开发。

		var http = require('http');
		http.createServer(function (req, res) {
  			res.writeHead(200, {'Content-Type': 'text/plain'});
  			res.end('Hello World\n');
		}).listen(1337, '127.0.0.1');
		console.log('Server running at http://127.0.0.1:1337/');

运行上述代码，打开浏览器，输入：`http://127.0.0.1:1337/`,接着眼前一亮，熟悉而又亲切的`Hello World`映入眼帘，惊喜，激动。事实上换做`Java`开发的话，这个时候很可能会胆战心惊的担心失败。

>注：这里的运行环境之类的东西，看完`node.js`是什么，应该已经搭建起来的。

与JavaScript结缘算是比较深的了，大学时候每个周五晚上都要到图书馆翻翻乱七八糟的书，统称为无用的书，因为与教学无关，考试无关，然而
恰恰是这些东西在不断的作用着我，并且产生一些影响。

****

学习过程.....(xxx 小时之后)

****

做Web应用既然上面代码已经提供了Web服务，接下里如何处理请求呢？

请求分为两类：**动态请求，静态请求**

*想到这里，我完全忽视了`node.js`本身的模块管理机制，代码组织方式，Web应用目录结构组织，外部引用，以及模块编写等等。*

完全凭借拙劣的`javascript`开发基础，`java`web应用开发的基本方式边学边开发。

这个时候，整体思路出来了，目的明确了：**系统处理静态资源请求，用户自定义逻辑处理动态请求**。


`NJW`的目录结构是：

>
+ public *客户端目录*
  + css **
  + html
  + images
  + js
  + `favicon.ico`
  + `index.html`
+ server *服务器的目录*
  + sys
     + `__hadnler__.js`
     + `__router__.js`
     + `__server__.js`
     + `404.html`
     + `500.html`
     + `cache.js`
     + `dynamic.js`
     + `mime.js`
     + `static.js`
     + `tools.js`
  + user
     + `app_handler.js`
     + `upload_handler.js`
+ `__config__.js`
+ `__main__.js`
+ `README.md`

这样的目录结构是学习`node.js`过程中按照`java Servlet`方式来的。

下面简单介绍几个模块的意思：

`__main__.js`:服务器启动模块，是整个Web应用的入口程序

`__config__.js`:应用程序的配置模块，定义了一些系统参数，	`handler`模块等，并对所有`handler`进行组织，以`handlers`统一管理。

`sys`:目录用户方法服务端有关服务器核心处理程序模块

+ `__router__.js`:router相当与Distribute,对请求进行一定的处理之后，分发给相应的handler处理。
+ `__handler__.js`:处理程序，请求处理程序是一组`handler`构成`handlers`,经过`router`转发后对应请求的`handler`进行逻辑处理。
+ `cache.js, dynamic.js, mime.js, static.js, tools.js`是	`handler`处理过程中依赖的一些模块。
+ `404.html, 500.html`服务器核心处理中内置的两个页面文件。
+ `dynamic.js`:经由`router`的的动态请求交由`dynamic`来进行`handler`	分发。
+ `static.js`:经由`router`的的动态请求交由`static`来进行处理。

`user`:目录这里存放用户的`handler`程序，主要是具体业务处理和动态请求处理程序。

+ `app_handler.js, upload_handler.js`:这些都是处理逻辑（这里仅作描述，无具体实现）



总的概括：

>整个应用最繁忙的部分是：router，router处理每一个请求，然后进行请求转发。

>整个应用最关键的部分是：handlers,handlers相当于Servlet集合,用来处理不同的请求并进行响应。

>整个应用配置信息部分是：__config__，其中配置的系统参数，最重要的是handler_path的配置，handler_path是由一组handler处理文件组成，每个handler处理问题提供了一组handler,一个handler相当与一个servlet处理程序。

至此，以`java servlet`开发方式完成了一个Web应用的开发模版，实现基本了静态资源请求，动态请求处理方式。同时，在经历这大大小小的问题，思考，查询，实验对`node.js`开发有了清晰的认识。

### 5. 总结与思考

`NJW`后的感触：

+ 4 中的方式不是规范的`node.js`开发，`node.js`有自己的方式。
+ `node.js`开发要跳出`java`编程的思想和模式（这个是针对熟悉`java`开发的同学）。
+ `node.js`并非这么简单，当然也并发那么高深莫测。

学习`node.js`需要更多了解：

+ `javascript`核心知识（并非客户端javascript）
+ 代码组织：`node.js`以`modules`的方式来组织代码。
+ 模块/包管理：[`npm`](https://www.npmjs.org/ "npmjs")`node.js`包管理和分发工具 (类似`java maven`工具)。
+ 异步I/O和事件驱动编程
+ 模块加载机制
+ `node.js`核心模块应用
+ [`AMD`](https://github.com/amdjs/amdjs-api/wiki/AMD "AMD规范")(Asynchronous Module Definition 异步模块定义)
+ [`CMD`](https://github.com/seajs/seajs/issues/242 "CMD规范")(Common Module Definition 通用模块定义)
+ [`CommonJS`](http://www.commonjs.org/ "CommonJS规范") 定义了应用程序使用`javascript`的API规范（并非浏览器）
+ 一些常用的第三方包，比如：web开发的`express`; 数据库操作的`mongodb, mongoose, mysql, mysql-api`; 缓存包`cache, obj-cache, mem-cache` 这些都可以通过[`npm`](https://www.npmjs.org/ "npmjs")得到。Node Js发展的速度非常块，第三方的模块每日剧增，这使得后来者可以站在前人的基础上然后继续创造。


最后：

了解Node Js之后，发现这个东西打破了一种固有的软件开发合作方式，前端开发和服务端开发没有了界限，同时让`javascript`的力量更大程度的发挥。前端，后端只是人为的划分而已，全栈的说法出现之后就已经标志二者之间无法扯清界限，当然也不可能扯清。

对于开发者而言，更不该有争夺城池之说，加入一个社区，就是信仰这个社区的文化，可以融合其它，但决不能对立，因为硬币也有可能立起来的。






