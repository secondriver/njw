/**
 * User: ZhangXiao
 * Date: 14-4-23
 * Time: 下午5:40
 * File: user/handler.js
 */


exports.handlers = {
    "/sys": sysInfo
};


var util=require("util");

function sysInfo(request, response, options) {
    response.writeHead(200, {
        "Content-Type": "text/json"
    });
    response.write(util.inspect(options), "utf8");
    response.end();
}