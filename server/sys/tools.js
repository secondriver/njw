/**
 * tools:
 * 工具类，不同于内置的util工具
 *
 * User: ZhangXiao
 * Date: 14-4-23
 * Time: 下午2:11
 * File: sys/tools.js
 */

exports.pushArray = function (thisArray, array) {
    pushArray.call(thisArray, array);
};

exports.pushMap = function (thisMap, map) {
    pushMap.call(thisMap, map);
};

exports.copyObject = copyObject;


/**
 * 复制对象
 * @param thisObject
 * @returns {*}
 */
function copyObject(thisObject) {
    var newObject;
    if (typeof thisObject === "object") {
        if (thisObject instanceof Array) {
            newObject = [];
            for (var i = 0, l = thisObject.length; i < l; i++) {
                newObject.push(thisObject[i]);
            }
        } else {
            newObject = {};
            for (var p in thisObject) {
                newObject[p] = copyObject(thisObject[p]);
            }
        }

    } else {
        newObject = thisObject;

    }
    return newObject;
}


/**
 * 数组合并
 *
 * @param array
 * @returns {*}
 */
function pushArray(array) {
    for (var i = 0, l = array.length; i < l; i++) {
        this.push(array[i]);
    }
    return this;
}

/**
 * 键值对合并
 *
 * @param map
 * @returns {*}
 */
function pushMap(map) {
    for (var k in map) {
        if (!this[k]) {
            this[k] = map[k];
        }
    }
    return this;
}