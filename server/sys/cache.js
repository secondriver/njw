/**
 * cache:
 * 简单的缓存处理模块
 *
 * User: ZhangXiao
 * Date: 14-4-23
 * Time: 下午6:47
 * File: sys/cache.js
 */

exports.cache = function () {

    var __cache__ = {

    };

    //默认过期时间为1天
    var _expired = 1000 * 60 * 60 * 24;

    function add(key, data, expired) {
        var long = new Date().getTime();
        __cache__[key] = {
            data: data,
            expired: ((isNaN(expired) || expired === null) ? _expired : expired != -1 ? (long + expired) : -1)
        };
    }

    function get(key) {
        clear();
        if (__cache__[key]) {
            return  __cache__[key];
        } else {
            return null;
        }
    }

    function remove(key) {
        if (__cache__[key]) {
            delete __cache__[key];
        }
    }

    function clear() {
        var long = new Date().getTime();
        var exp;
        for (var key in __cache__) {
            exp = __cache__[key].expired
            if (exp != -1) {
                if (long <= exp) {
                    delete __cache__[key];
                }
            }
        }
    }

    return {
        add: add,
        get: get,
        remove: remove
    }
}





