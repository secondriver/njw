/**
 * dynamic:
 * 动态请求处理程序主模块
 *
 * User: ZhangXiao
 * Date: 14-4-24
 * Time: 下午4:38
 * File: sys/dynamic.js
 */

var util = require("util");
var path = require("path");
var tools = require("./tools");

var _options = {};
//缓存对象
var _handlers = {};
var _public = ".";
var _approot = ".";


//主处理方法
function dynamicHandler(pathname, request, response, options) {
    _options = tools.copyObject(options);
    _handlers = _options.handlers;
    _approot = _options.approot;
    _public = path.resolve(_options.approot, _options.app.client);

    util.log("Dynamic request url is:" + request.url + " and pathname is:" + pathname);

    if (typeof _handlers[pathname] === "function") {
        _handlers[pathname](request, response, _options);
    } else {
        _handlers["handler_404"](request, response, _options);
    }
}

exports.dynamic = dynamicHandler;