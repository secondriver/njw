/**
 * __server__:
 * Web应用核心程序，服务器程序
 *
 * User: ZhangXiao
 * Date: 14-4-23
 * Time: 上午11:59
 * File: sys/__server__.js
 */


var http = require("http");
var url = require("url");
var router = require("./__router__").router;
var util = require("util");
var tools = require("./tools");

var _options = {};

//启动服务
function __run__(options) {

    _options = tools.copyObject(options);

    var server = http.createServer(function (request, response) {
        router(request, response, _options);
    });

    var port = _options.server.port;
    server.listen(port, function () {
        util.log("NodeJs Server running at http://localhost:" + port + "/ ...");
    });
}

//停止服务
function __stop__() {
    util.log("NodeJs Server Stop ...");
}

//重启服务
function __restart__() {
    util.log("NodeJs Server Restart ...");
}


exports.server = {
    run: __run__,
    stop: __stop__,
    restart: __restart__
};

