/**
 * __config__：
 *
 * 定义了系统的全局配置信息
 *
 * User: ZhangXiao
 * Date: 14-4-23
 * Time: 下午12:00
 * File: __config__.js.
 */

/**
 *
 * 系统配置分为3个部分：
 *
 * app:
 * 主要说明：server:存放服务程序，client:存储公开访问的文件
 *
 * server:
 * 主要说明：系统默认的服务程序配置,root:系统服务程序目录，port:服务启动端口
 *
 * user:
 * 主要说明：用户服务程序配置，root:用户服务程序目录，handler_path:用户服务处理程序模块集合
 *
 * client:
 * 主要说明：配置公开访问文件的相关参数，比如:欢迎页面(页面要以"/"根开头)
 *
 */
/*********************************以下部可以根据实际情况配置********************************/
var __config__ = {
    app: {
        name: "Application name",
        version: "Application version.",
        description: "Application description.",
        server: "./server",
        client: "./public"
    },
    server: {
        root: "sys",
        port: 8088
    },
    user: {
        root: "user",
        handler_path: ["app_handler.js", "upload_handler.js"]
    },
    client: {
        home: "/index.html"
    }
};

/*********************************以下部分不建议随意改动**********************************/
var util = require("util");
var path = require("path");
var fs = require("fs");

//服务器核心程序根目录
var sys = __config__.app.server + "/" + __config__.server.root;
//服务器用户程序根目录
var user = __config__.app.server + "/" + __config__.user.root;

//服务器核心程序tools模块
var tools = require(sys + "/tools.js");
var main = sys + "/__server__.js";

//Web应用处理程序集（加载服务器核心处理程序）
var __handlers__ = require(sys + "/__handler__").handlers;

//Web应用用户自定义的处理程序模块集
var __user__handlers__module__ = __config__.user.handler_path;

//服务启动一次性加载系统中的用户自定义的handlers
if (util.isArray(__user__handlers__module__)) {
    for (var i = 0, l = __user__handlers__module__.length; i < l; i++) {
        var module = user + "/" + __user__handlers__module__[i];
        var handler = require(module).handlers;
        tools.pushMap(__handlers__, handler);
    }
}
//handler:系统中的handler集合
__config__.handlers = __handlers__;

//系统的根目录的绝对路径.
var __app__root__ = fs.realpathSync(".");
//_approot:系统的根目录的绝对路径
__config__.approot = __app__root__;

//服务程序系统目录
__config__.sys = sys;

//服务程序用户目录
__config__.user = user;

//服务程序系统主模块路径
__config__.main = main;

//服务程序系统内置工具模块
__config__.tools = tools;

//打印系统配置加载日志
util.log("Start loading system configuration information...");
util.log(util.inspect(__config__, {showHidden: false}));
util.log("Over loading  system configuration information...");

//导出系统配置
exports.config = __config__;
